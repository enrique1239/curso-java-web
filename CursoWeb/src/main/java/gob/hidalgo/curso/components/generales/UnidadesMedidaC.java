package gob.hidalgo.curso.components.generales;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.ClienteEO;
import gob.hidalgo.curso.database.generales.UnidadMedidaEO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("UnidadesMedidaC")
public class UnidadesMedidaC {

	@Autowired 
	private SqlSession sqlSession;
	
	@Autowired
	private MensajesC mensajesC;
	
	public UnidadesMedidaC() {
		super();
		log.debug("Se crea componente UnidadesMedidaC");
	}
	
	public Modelo<UnidadMedidaEO> modelo(){
		List<UnidadMedidaEO> listado;
		listado = sqlSession.selectList("generales.unidadesMedida.listado");
		return new Modelo<UnidadMedidaEO>(listado);
	}
	
	public UnidadMedidaEO nuevo() {
		return new UnidadMedidaEO();
	}
	
	public boolean guardar(UnidadMedidaEO unidad) {
		if(unidad.getId() == null) {
			sqlSession.insert("generales.unidadesMedida.insertar", unidad);
			mensajesC.mensajeInfo("Unidad agregada exitosamente");
		} else {
			sqlSession.update("generales.unidadesMedida.actualizar", unidad); 
			mensajesC.mensajeInfo("Unidad actualizada exitosamente");
		}
		return true;
	}
	
	public boolean eliminar(UnidadMedidaEO unidad) {
		sqlSession.insert("generales.unidadesMedida.eliminar", unidad);
		mensajesC.mensajeInfo("Unidad eliminada exitosamente");
		return true;
	}
	
}
