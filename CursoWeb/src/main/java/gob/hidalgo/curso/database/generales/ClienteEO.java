package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("ClienteEO")
public class ClienteEO extends EntityObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String curp;
	@NotBlank(message = "Ingrese el nombre")
	private String nombre;
	@NotBlank(message = "Ingrese el primer apellido")
	private String primerApellido;
	@NotBlank(message = "Ingrese el segundo apellido")
	private String segundoApellido;
	@NotNull(message = "Seleccione la fecha de nacimiento")
	@PastOrPresent(message = "Seleccione una fecha de nacimiento valida")
	private LocalDate fechaNacimiento;
	private String mail;
	@NotBlank(message = "Ingrese el numero celular")
	private String celular;
	@JsonIgnore
	private String fechaNacimientoT;
	
	private List<OrdenPagoEO> ordenesPago;
	
	public String getFechaNacimientoT() {
		if(fechaNacimientoT == null) {
			if(fechaNacimiento != null) {
				fechaNacimientoT = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			} else {
				fechaNacimientoT = "";
			}
		}
		return fechaNacimientoT;
	}
}
